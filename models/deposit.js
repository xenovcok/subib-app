const mongoose = require('mongoose');

var Schema =  mongoose.Schema;

var transaksiSchema = new Schema({
	id_order:String,
	no_akun:String,
	nama:String,
	bank_tujuan:String,
	nominal:Number,
	total_transfer:Number,
	reg_date:String,
	proc_date:String,
	status:String,
	ticket:String,
	jenis:String,
	bank:String,
	no_rek:String
});

module.exports.Transaksi = mongoose.model('Transaksi', transaksiSchema);