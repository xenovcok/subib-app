const mongoose = require('mongoose');

var Schema =  mongoose.Schema;

var userSchema = new Schema({
	no_akun:String,
	nama:String,
	email:String,
	phone:Number,
	phonepass:String,
	tuj_rebate:String,
	bank:String,
	no_rek:String,
	reg_date:String,
	aff:String
});

module.exports.User = mongoose.model('User', userSchema);