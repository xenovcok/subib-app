var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var rebateSchema =  new Schema({
	no_akun:String,
	status:String,
	nama:String,
	nominal:String,
	tujuan_rebate:String,
	bank:String,
	no_rek:String,
	process_date:String,
	ticket:String
});

module.exports.Rebate = mongoose.model('Rebate', rebateSchema);