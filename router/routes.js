var mongoose =  require('mongoose');
var User = require("../models/user.js").User;
var Transaksi = require("../models/deposit.js").Transaksi;
var Rebate = require("../models/rebate.js").Rebate;

module.exports = function(app){

	function tgl(){
		var d = new Date();
		var curr_date = d.getDate();
		var curr_month = d.getMonth();
		var curr_year = d.getFullYear();
		var naw = curr_year+"/"+curr_month+"/"+curr_date;

		return naw;
	}

	app.get('/', function(req, res){
		res.render('index.html');
	});
// user api
app.get('/api/members/:id', function(req, res){
	User.findById(req.params.id, function(err, data){
		if(err) throw err;
		res.json(data);
	});
});

app.get('/api/members', function(req, res){
	User.find({}, function(err, data){
		if(err) throw err;
		res.json(data);
	});
});

app.put('/api/members/:id', function(req, res){
	User.findById(req.params.id, (err, data) => {
		var user = req.body;

		data.no_akun = user.no_akun;
		data.nama = user.nama;
		data.email = user.email;
		data.phone = user.phone;
		data.phonepass = user.phonepass;
		data.tuj_rebate = user.tuj_rebate;
		data.bank = user.bank;
		data.no_rek = user.no_rek;
		data.reg_date = tgl();
		data.aff = "SUPER";

		data.save(function(err){
			if(err)
				throw err;

			res.json({"message":"sukses"});
		});

	});
});

app.post('/api/members', function(req, res){
	var data = new User();

	data.no_akun = req.body.no_akun;
	data.nama = req.body.nama;
	data.email = req.body.email;
	data.phone = req.body.phone;
	data.phonepass = req.body.phonepass;
	data.tuj_rebate = req.body.tuj_rebate;
	data.bank = req.body.bank;
	data.no_rek = req.body.no_rek;
	data.reg_date = tgl();
	data.aff = "SUPER";

	data.save(function(err){
		if(err)
			throw err;

		res.json({"message":"sukses"});
	});


});

app.delete('/api/members/:id', (req, res) => {
	User.remove({
		_id : req.params.id
	}, function(err, data){
		if(err)
			res.send(err)
		res.json(data);
	});
});
// end user API

// Deposit API
app.post('/api/transaksi/depo', (req, res) => {

	var data = req.body;
	var trans = new Transaksi();

	User.find({no_akun : data.no_akun}, (err, user) => {

		trans.id_order = "DP-"+String(Math.abs(Math.round(Math.random() * (999999-100000) + 1)));
		trans.no_akun = data.no_akun;
		trans.bank_tujuan = data.bank_tujuan;
		trans.nominal = data.nominal;
		trans.total_transfer = data.jml_transfer;
		trans.reg_date = now();
		trans.nama = user[0].nama;
		trans.bank = user[0].bank;
		trans.no_rek = user[0].no_rek;
		trans.proc_date = "";
		trans.status = "NEW";
		trans.ticket = Math.abs(Math.round(Math.random() * (999999-100000) + 1));
		trans.jenis = "DP";

		trans.save(function(err){
			if(err)
				throw err;

			res.json({"message":"sukses"});
		});
	});
});

app.get('/api/transaksi/depo', (req,res) => {
	Transaksi.find({jenis : "DP"}, (err, data) => {	
		res.json(data);
	});
});

app.get('/api/json', (req,res) => {
	Transaksi.find({jenis : "DP"}, (err, data) => {
		res.json(data);
	});
});

app.get('/api/transaksi/wd', (req,res) => {
	Transaksi.find({jenis : "WD"}, (err, data) => {
		res.json(data);
	});
});

app.get('/api/transaksi/:id', (req,res) => {
	Transaksi.findById( req.params.id, (err, data) => {
		res.json(data);
	});
});

function now(){
	var d = new Date();
	var now = d.getFullYear()+"/"+d.getMonth()+"/"+d.getDate()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();

	return now;
}

app.put('/api/proses/:id', (req, res) => {
	Transaksi.findById(req.params.id, (err, trans)=> {
		var data = req.body;

		trans.id_order = data.id_order;
		trans.no_akun = data.no_akun;
		trans.bank_tujuan = data.bank_tujuan;
		trans.nominal = data.nominal;
		trans.bank = data.bank;
		trans.no_rek = data.no_rek;
		trans.total_transfer = data.total_transfer;
		trans.proc_date = now();
		trans.status = data.status;
		trans.ticket = data.ticket;
		trans.jenis = "DP";

		trans.save(function(err){
			if(err)
				throw err;

			res.json({"message":"sukses"});
		});
	});
});

app.delete('/api/transaksi/:id', (req, res) => {
	Transaksi.remove({
		_id : req.params.id
	}, function(err, data){
		if(err)
			res.send(err)
		res.json(data);
	});
});

//rebate API
app.get('/api/rebate', (req, res) => {
	Rebate.find({}, (err, data) => {
		res.json(data);
	});
});

app.post('/api/rebate', (req, res) => {
	var rbt = new Rebate();
    var dat = req.body;

	rbt.no_akun = dat.no_akun;
	rbt.nama = dat.nama;
	rbt.status = "NEW";
	rbt.nominal = dat.nominal;
	rbt.tujuan_rebate = dat.tuj_rebate;
	rbt.bank = dat.bank;
	rbt.no_rek = dat.no_rek;
	rbt.process_date = now();
	rbt.ticket = "RBT-"+String(Math.abs(Math.round(Math.random() * (999999-100000) + 1)));
});

}