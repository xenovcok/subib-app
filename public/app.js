(function(){

	'use strict';

	var app = angular.module('subibApp', [
		'ui.router',
		'datatables'
		]);

	app.config(function($stateProvider, $urlRouterProvider){
		$urlRouterProvider.otherwise("/");

		$stateProvider
		.state('user', {
			url : '/user',
			templateUrl : 'app/user.html',
			controller : 'userController'
		})
		.state('addUser', {
			url : '/user/add',
			templateUrl : 'app/user.form.html',
			controller : 'userController'
		})
		.state('editUser', {
			url : '/user/edit/:id',
			templateUrl : 'app/user.edit.form.html',
			controller : 'userEditController'
		})
		.state('deposit', {
			url : '/deposit',
			templateUrl : 'app/trans/deposit.list.html',
			controller : 'transListController'
		})
		.state('withdraw', {
			url : '/withdraw',
			templateUrl : 'app/trans/withdraw.list.html',
			controller : 'wdlistController'
		})
		.state('proses', {
			url : '/proses/:id',
			templateUrl : 'app/trans/edit.form.html',
			controller : 'prosesTransController'
		}).state('rebate', {
			url : '/rebate',
			templateUrl : 'app/rebate/rebate.list.html',
			controller : 'rebateListController'
		});
	});
})();