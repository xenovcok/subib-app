angular.module('subibApp').controller('userEditController', function($scope, $http, $stateParams, $state) {
	var id = $stateParams.id;
	console.log(id);


	$http.get('/api/members/'+id)
	.success((data) => {
		$scope.formData = data;
		//console.log(data);
	});

	$scope.simpan = function(id){
		$http.put('/api/members/'+id, $scope.formData)
		.success((data) => {
			$scope.formData = {};
			$scope.success = true;
			$state.go('user');
		});
	}


});