angular.module('subibApp').controller('rebateListController', function($scope, $http, $stateParams, $state){
	loadData();

	function loadData(){
		$http.get('/api/rebate').
		success((data, status, headers, config) => {
			$scope.trans = data;
		}).
		error((data, status, headers, config) => {
			console.log(status);
		});
	}

	$scope.del = (id) => {
		$http.delete('/api/rebate/'+id).
		success((data) => {
			$scope.trans = data;
			loadData();
		}) ;
	}
});