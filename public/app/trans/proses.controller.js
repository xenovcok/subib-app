angular.module('subibApp').controller('prosesTransController', function($state, $scope, $stateParams, $http){
	var id = $stateParams.id
	$http.get('/api/transaksi/'+id)
	.success((data) => {
		console.log(data);
		$scope.formData = data;
	});

	$scope.update = function(id){
		$http.put('/api/proses/'+id, $scope.formData)
		.success((data) => {
			$scope.formData = {};
			$state.go('deposit');
		});
	}
});