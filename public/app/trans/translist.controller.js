angular.module('subibApp').controller('transListController', function($scope, $http, $stateParams, $state){
	loadData();

	function loadData(){
		$http.get('/api/transaksi/depo').
		success((data, status, headers, config) => {
			$scope.trans = data;
		}).
		error((data, status, headers, config) => {
			console.log(status);
		});
	}

	$scope.del = (id) => {
		$http.delete('/api/transaksi/'+id).
		success((data) => {
			$scope.trans = data;
			loadData();
		}) ;
	}
});