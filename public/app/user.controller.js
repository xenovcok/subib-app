angular.module('subibApp').controller('userController', function($scope, $http, $stateParams, $state) {
	loadUsers();

	function loadUsers(){
		$http.get('/api/members').
		success((data, status, headers, config) => {
			$scope.users = data;
		}).
		error((data, status, headers, config) => {
			console.log(status);
		});
	}

	$scope.delUser = function(id) {
		$http.delete('/api/members/' + id)
		.success(function(data) {
			$scope.data = data;
			console.log(data);
			loadUsers();
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});
	};

	$scope.createUser =  (() => {
		$http.post('/api/members', $scope.formData)
		.success(function(data){
			$scope.formData = {};
			$scope.success = true;
			console.log(data);
		})
		.error(function(data){
			console.log("error");
		});
	});
/*
	$scope.submitEdit = ((id) => {
		$http.put('/api/members/'+id, $scope.formData)
		.success((data, status, headers, config) => {
			$state.go('editUser');
			$scope.formData = data;
		})
		.error((data, status, headers, error) => {
			console.log(status);
		});
	})
*/

	//$scope.message="controller loaded";


	/*const userService = Restangular.one('api').all('members');
	const ctrl = this;

	loadUsers();


	function loadUsers() {
		userService.getList().then((users) => {
			ctrl.users = users;
		});

}*/
});