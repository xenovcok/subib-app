const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const mongoose =  require('mongoose');
const morgan = require('morgan');

const app = express();

app.use(express.static(__dirname + '/views'));
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/node_modules'));

mongoose.Promise = global.Promise;
var connection_string = '127.0.0.1:27017/subibdb';
// if OPENSHIFT env variables are present, use the available connection info:
mongoose.connect(connection_string);

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


require('./router/routes')(app);

app.get('*', function(req, res){
	res.redirect('/');
});

app.listen(8282, function(res, req){
	console.log('app listening port : 8282');
});